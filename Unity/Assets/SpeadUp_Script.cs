﻿using UnityEngine;
using System.Collections;

public class SpeadUp_Script : MonoBehaviour 
{
	private PlayerMovement _newSpeed;
	public float speed;
	public float time;
	
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player")
		{
			_newSpeed = other.GetComponent<PlayerMovement>();
			_newSpeed.ModifySpeed(speed, time);
			Destroy(transform.gameObject);
		}
		if ((other.tag == "Death") && (other.GetType()==typeof(CapsuleCollider)) )
		{
			Destroy(transform.gameObject);
		}
	}
}
