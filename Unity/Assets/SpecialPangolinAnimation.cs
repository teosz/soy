﻿using UnityEngine;
using System.Collections;

public class SpecialPangolinAnimation : MonoBehaviour
{
	private Sprite[] sprites;
	public string spriteSheetLocation;
	private SpriteRenderer _spriteRenderer;
	public int specificFramesStart;
	public bool isAnimating = false;
	private float _animStartingTime;
	private Vector3 _animStartingPoint;
	private Vector3 _animEndPoint;
	public float maxY;
	public float animationTime = 1;
	private SpriteManager _spriteScript;
	public float fraction = 0.166F; // when shall the pangolin "open", is apllied to whole anim time

	void Awake ()
	{
		_spriteRenderer = GetComponent<SpriteRenderer> ();
		sprites = Resources.LoadAll<Sprite>(spriteSheetLocation);
		_spriteRenderer.sprite = sprites [specificFramesStart];
		_spriteScript = GetComponent<SpriteManager> ();
	}

	void Update()
	{
		if (isAnimating) {
			if(Time.time - _animStartingTime > animationTime) {
				isAnimating = false;
				_spriteScript.SetSpecific(false);
				transform.position = _animStartingPoint;
			} else {
				if (Time.time - _animStartingTime > fraction * animationTime) {
					_spriteRenderer.sprite = sprites[specificFramesStart + 1];
				}
			transform.position = Vector3.Lerp(_animStartingPoint,_animEndPoint,Mathf.Sin((Time.time-_animStartingTime)*Mathf.PI/animationTime));
			}
		}
	}

	public void StartAnim()
	{
		_animStartingTime = Time.time;
		_animStartingPoint = transform.position;
		_animEndPoint = transform.position;
		_animEndPoint.y += maxY;
		isAnimating = true;
	}
}
