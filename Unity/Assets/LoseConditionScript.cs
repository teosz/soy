using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoseConditionScript : MonoBehaviour {

	// Use this for initialization
	private bool isWin;
	void Start () {
		isWin  = false;
	}

	// Update is called once per frame
	void Update () {
		if( !isWin )
		{
			if(GameObject.FindGameObjectsWithTag("Player").Length == 0)
			{
				GetComponent<Text>().text = "YOU LOST";
			}
			else
			{
				GetComponent<Text>().text = "";
			}
		}
	}
	public void win(){
		isWin = true;
		GetComponent<Text>().text = "YOU WON";

	}
}
