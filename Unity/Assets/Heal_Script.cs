﻿using UnityEngine;
using System.Collections;
using Active;

public class Heal_Script : MonoBehaviour 
{
	private ItemCollector itemCollector;


	public int heal_points=25;


	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player")
		{
			GameObject.Find("HealthManager").GetComponent<HealthManager> ().modifyHealth(heal_points, other.gameObject);
			Destroy(transform.gameObject);
		}
	}
}
