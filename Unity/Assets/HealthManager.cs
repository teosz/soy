﻿using UnityEngine;
using System.Collections;
using Active;
public class HealthManager : MonoBehaviour {

	void Start () {
		new Characters();

	}
	[RPC]
	void modifyHealthAction(int health, int id)
	{
		Characters.modifyHealthInternal(health,id);
	}
	public void modifyHealth(int health, GameObject character)
	{

		PhotonView photonView = PhotonView.Get(this);
		photonView.RPC("modifyHealthAction", PhotonTargets.All, health, character.GetInstanceID());
	}
	public void pushPlayer(GameObject character, int life)
	{
		Characters.pushPlayer(character, life);	
	}

}
