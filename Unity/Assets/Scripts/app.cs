using System;
using Initialize;
using UnityEngine;
using System.Text.RegularExpressions;
using Active;
public class app : Photon.MonoBehaviour
{
  private Initializator InitializatorInstance;
  public GameObject playerPrefab;
  public Vector3 Spawn;
  private CameraMovement playerCamera;
  private GameObject ActivePlayer;
  public int playerBaseHP = 200;
  public int enemyBaseHP = 50;

  private System.Random rnd;

  public void Start()
  {
    InitializatorInstance = gameObject.AddComponent<Initializator> ();
    InitializatorInstance.init(delegate() { _playerDidConnect(); }, delegate() { _roomDidCreate(); });

  }
  private void _playerDidConnect()
  {

    GameObject[] gos;
    gos = GameObject.FindGameObjectsWithTag("Enemy");
    foreach (GameObject go in gos) {
      go.SetActive(false);
    }
    _spwanPlayer();

  }
  private void _roomDidCreate ()
  {
    GameObject[] gos;
    gos = GameObject.FindGameObjectsWithTag("Enemy");


    foreach (GameObject go in gos) {
      Match match = Regex.Match(go.name, @"(\w+)\s?(\d+)?");
      GameObject p = PhotonNetwork.Instantiate(match.Groups[1].Value, go.transform.position, Quaternion.identity, 0);
      p.tag = "ActiveEnemy";
	  Characters.pushPlayer(p, enemyBaseHP);

    }

  }
  public GameObject getActivePlayer()
  {
    return ActivePlayer;
  }
  private  void _spwanPlayer()
  {

    rnd = new System.Random();
    String name = "Player-" + rnd.Next(999999);
    ActivePlayer = (GameObject) PhotonNetwork.Instantiate(playerPrefab.name, Spawn, Quaternion.identity, 0);
    ActivePlayer.name = name;
    ActivePlayer.SetActive(true);
	Characters.pushPlayer(ActivePlayer, playerBaseHP);

    GameObject CameraObject = GameObject.Find("Main Camera");


    CameraObject.SetActive(true);
    playerCamera = (CameraMovement) CameraObject.GetComponent(typeof(CameraMovement));
    playerCamera.target = ActivePlayer.transform;


  }
}
