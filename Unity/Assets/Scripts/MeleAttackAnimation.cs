﻿using UnityEngine;
using System.Collections;

public class MeleAttackAnimation : MonoBehaviour// de sters acest script
{
	SpriteManager _spriteManager;
	public ObjectChecker _objFinder;

	int _reapeatingIterator = 0;
	int _internIndex;

	bool isChargeing = false;
	bool isAttacking = false;

	void Awake ()
	{
		_spriteManager = GetComponent<SpriteManager> ();
	}

	 void SetChargeImage()
	{
		_spriteManager.SetSpecific(true);

		_internIndex = _spriteManager.SetIndexRelativeToCameraRotation ();
	
		_internIndex += _spriteManager.numberOfFrames.staying +
			_spriteManager.numberOfFrames.moving +
			_spriteManager.numberOfFrames.perAttack * _reapeatingIterator;

		_spriteManager.SetSpecificImage (_internIndex);

		_objFinder.SetTheirColor (_objFinder.objects, Color.yellow);
	}

	void SetAttackImage()
	{
		_spriteManager.SetSpecific(true);


		
		_internIndex = _spriteManager.SetIndexRelativeToCameraRotation ();
		
		_internIndex += _spriteManager.numberOfFrames.staying +
						_spriteManager.numberOfFrames.moving +
						_spriteManager.numberOfFrames.perAttack * _reapeatingIterator;

		_internIndex++;

		_spriteManager.SetSpecificImage (_internIndex);

		_objFinder.SetTheirColor (_objFinder.objects, Color.yellow);
	}

	void Update()
	{
		if (isChargeing) {
			SetChargeImage ();
		} else if (isAttacking) {
			SetAttackImage();
		}
	}

	public void StartCharging()
	{
		isChargeing = true;
		isAttacking = false;

		if (_reapeatingIterator == 0)
			_reapeatingIterator = 1;
		else
			_reapeatingIterator = 0;

		//++_reapeatingIterator;
		//_reapeatingIterator %= _spriteManager.numberOfFrames.attacking /
		//	_spriteManager.numberOfFrames.perAttack ;
	}

	public void StartFastCombo() 
	{
		isChargeing = false;
		isAttacking = true;


		//_reapeatingIterator %= _spriteManager.numberOfFrames.attacking /
		//	_spriteManager.numberOfFrames.perAttack ;
	}

	public void EndAttack()
	{
		_spriteManager.SetSpecific(false);

		_objFinder.SetTheirColor (_objFinder.objects, Color.white);

		isChargeing = false;
		isAttacking = false;
	}

}
