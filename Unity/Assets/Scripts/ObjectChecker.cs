﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectChecker : MonoBehaviour
{
	public HashSet<GameObject> objects = new HashSet<GameObject>();
	HashSet<GameObject> _intermediateHash = new HashSet<GameObject>();
	public HashSet<GameObject> leavers = new HashSet<GameObject>();
	public string friendlyTag;
	public bool toColor = false;

	void Update()
	{
		leavers.Clear ();
		foreach (GameObject obj in objects) {
			if(_intermediateHash.Contains(obj) == false){
				leavers.Add(obj);
			}
		}

		if (toColor) {
			SetTheirColor (leavers, Color.white); //FIXME aici daca da de cineva colorat pt altceva in ceva (de exemplu noi avem acum arcasi desenati ca mele rosii, ii decoloreaza
		}

		objects.Clear ();
		foreach (GameObject obj in _intermediateHash) {
			objects.Add(obj);
		}

		//SetTheirColor (objects, Color.yellow);
		_intermediateHash.Clear ();
	}

	public void SetTheirColor(HashSet<GameObject> hashSet ,Color newColor)
	{
		Transform image;
		foreach (GameObject obj in hashSet) {
			if((obj.tag == "ActiveEnemy" || obj.tag == "Player") && obj.tag != friendlyTag) {
				image = obj.transform.Find("Image");
				if(image != null){
						image.GetComponent<SpriteRenderer>().color = newColor;
				}
			}
		}
	}

	void OnTriggerStay(Collider other)
	{
		if (!other.isTrigger && !_intermediateHash.Contains(other.gameObject)) {
			_intermediateHash.Add(other.gameObject);
		}
	}
}
