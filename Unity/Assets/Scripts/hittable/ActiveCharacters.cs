using System;
using System.Collections.Generic;
using UnityEngine;

namespace Active
{
  public class Characters
  {

    private struct _structuralCharacter
    {
      public GameObject reference;
      public int health;
      public int mana;
    }
    private static List<_structuralCharacter> _characters;
    private static List<int> _IndexCharacters;
    public Characters()
    {
      if(_characters == null)
      {
        _characters = new List<_structuralCharacter>();
        _IndexCharacters = new List<int>();
      }
    }
    [RPC]
    public  static void pushPlayer(GameObject NewPlayer, int life=100)
    {
      int id = NewPlayer.GetInstanceID();
      if(_IndexCharacters.IndexOf(id) == -1)
      {
        _structuralCharacter obj;
        obj.reference = NewPlayer;
        obj.health = life;
        obj.mana = 100;
        _characters.Add(obj);
        _IndexCharacters.Add(id);
      }

    }
    [RPC]
    public static void modifyHealthInternal(int health, int id )
    {
      int CharacterIndex =  _IndexCharacters.IndexOf(id);
      _structuralCharacter obj = _characters[CharacterIndex];
      obj.health += health;
      if(obj.health <= 0)
      {
        obj.reference.SetActive(false);
        obj.health = 0;
      }
      _characters[CharacterIndex] = obj;
    }
    [RPC]
    public static int getHealth(GameObject character)
    {
			int CharacterIndex =  _IndexCharacters.IndexOf(character.GetInstanceID()); /*FIXME NullReferenceException: Object reference not set to an instance of an object Active.Characters.getHealth (UnityEngine.GameObject character) (at Assets/Scripts/hittable/ActiveCharacters.cs:53) lifebar.Update () (at Assets/Scripts/lifebar.cs:18)*/
      if(CharacterIndex != -1)
        return  _characters[CharacterIndex].health;
      return 1;
    }


  }
}
