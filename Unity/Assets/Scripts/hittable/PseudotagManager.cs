﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PseudotagManager : MonoBehaviour
{
	public Dictionary<string, bool> pseudotags;
	public float id;
	private static float idCounter = 0;

	void Awake()
	{
		id = idCounter++;
	}
}
