using UnityEngine;
using System.Collections;

public class PlayerManager : Photon.MonoBehaviour
{
	private MeleAttackScript2 _attackingScript;
	private PlayerMovement _movingScript;
	private HealthManager _health;

	int floorMask;

	private float lastSynchronizationTime = 0f;
	private float syncDelay = 0f;
	private float syncTime = 0f;
	private Vector3 syncStartPosition = Vector3.zero;
	private Vector3 syncEndPosition = Vector3.zero;
	public KeyCode HealthCheat;
	private bool _first;

	public KeyCode front;
	public KeyCode left;
	public KeyCode back;
	public KeyCode right;




	void Start ()
	{

		_first = true;
		_attackingScript = GetComponent<MeleAttackScript2> ();
		_movingScript = GetComponent<PlayerMovement> ();
		_health = GameObject.Find("HealthManager").GetComponent<HealthManager> ();
		floorMask = LayerMask.GetMask ("TerrainToClick");
	}

	// Update is called once per frame
	void Update ()
	{
		

		RaycastHit hit;
		if(photonView.isMine)
		{
			if(Input.GetMouseButton(1)) {
				if(_attackingScript.isAttacking == false) {
					_attackingScript.ChargeAttack();

					Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
					if (Physics.Raycast(ray, out hit,Mathf.Infinity, floorMask)) {
						_movingScript.LookAtOnXZ(hit.point);
					}
				}
			} else if (_attackingScript._isCharging== true){
				_attackingScript.StartAttack();
			}

			if (_attackingScript.isAttacking == false)
			{
				if (Input.GetMouseButton(0)) {
					Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
					if (Physics.Raycast(ray, out hit,Mathf.Infinity, floorMask))
						_movingScript.MoveToPoint(hit.point);
				} else {
					Vector3 relativeDirection = new Vector3(0,0,0);
					if(Input.GetKey(front)) {
						relativeDirection.z += 1;
					}
					if(Input.GetKey(back)) {
						relativeDirection.z -= 1;
					}
					if(Input.GetKey(left)) {
						relativeDirection.x -= 1;
					}
					if(Input.GetKey(right)) {
						relativeDirection.x += 1;
					}
					relativeDirection.Normalize();
					_movingScript.WASDMove(relativeDirection);
				}
			}
		}
		else
		{
			SyncedMovement();
		}
		if(Input.GetKeyDown(HealthCheat)) {
			_health.modifyHealth(100,this.gameObject);
		}
	}
	public bool V3Equal(Vector3 a, Vector3 b){
		return Vector3.SqrMagnitude(a - b) < 0.1;
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			stream.SendNext(GetComponent<Rigidbody>().position);
			stream.SendNext(GetComponent<Rigidbody>().velocity);
		}
		else
		{
			Vector3 syncPosition = (Vector3)stream.ReceiveNext();
			Vector3 syncVelocity = (Vector3)stream.ReceiveNext();

			syncTime = 0f;
			syncDelay = Time.time - lastSynchronizationTime;
			lastSynchronizationTime = Time.time;

			syncEndPosition = syncPosition + syncVelocity * syncDelay;
			syncStartPosition = GetComponent<Rigidbody>().position;

		}
	}

	private void SyncedMovement()
	{
		if(!(syncEndPosition.Equals(Vector3.zero) && syncStartPosition.Equals(Vector3.zero)) && _first == true)
		{
			_first = false;
			GetComponent<Rigidbody>().position = syncEndPosition;
		}
		else if(!V3Equal(GetComponent<Rigidbody>().position,syncEndPosition))
	  {

			syncTime += Time.deltaTime;
			_movingScript.MoveToPoint(Vector3.Lerp(syncStartPosition, syncEndPosition, syncTime / syncDelay));
		}
	}
}
