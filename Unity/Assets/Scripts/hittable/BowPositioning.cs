﻿using UnityEngine;
using System.Collections;

public class BowPositioning : MonoBehaviour
{
	public Transform arrowSpawn;
	private Transform _imageTransform;
	public float distanceFromImage;
	public float notShoutingAngle;
	public float currentAngle;
	// private RangedAttackScript _rangedAttackScript;
	// Use this for initialization
	void Awake ()
	{
		// _rangedAttackScript = GetComponentInChildren<RangedAttackScript> ();
		_imageTransform = GetComponentInChildren<SpriteRenderer> ().transform;
		currentAngle = notShoutingAngle;
	}

	public void SetAngle (Vector3 targetImagePosition, float startingSpeed)
	{

	}

	// Update is called once per frame
	void FixedUpdate ()
	{
			Vector3 newPosition;
			newPosition.x = 0;
			newPosition.y = _imageTransform.localPosition.y + distanceFromImage * Mathf.Sin(notShoutingAngle * Mathf.Deg2Rad);
			newPosition.z = distanceFromImage * Mathf.Cos(notShoutingAngle * Mathf.Deg2Rad);
			arrowSpawn.localPosition = newPosition;
		arrowSpawn.localRotation = Quaternion.Euler(-currentAngle, 0, 0);
	}
}
