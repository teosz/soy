﻿using UnityEngine;
using System.Collections;
using Active;

public class RangedAIBrain : MonoBehaviour
{


	private AIMovement _moverScript;
	private AutoChoosingTarget _targetingScript;
	private RangedAttackScript _attackingScript;
	private SpriteManager _spriteScript;
	public float desiredRangedFromTarget = 1;

	Vector3 desiredSpot = new Vector3();

	Vector2 Vector3ToXZ(Vector3 v)
	{
		return new Vector2 (v.x, v.z);
	}

		void Awake ()
		{
			_moverScript = GetComponent<AIMovement> ();
			_targetingScript = GetComponent<AutoChoosingTarget> ();
			_attackingScript = GetComponentInChildren<RangedAttackScript> ();
			_spriteScript = GetComponentInChildren<SpriteManager> ();
		}

		void FixedUpdate()
		{
			if (_targetingScript.target) {
				if (Vector2.Distance (Vector3ToXZ (transform.position), Vector3ToXZ (_targetingScript.target.transform.position)) <= _attackingScript.attackingDistance) {
				_attackingScript.Attack (_targetingScript.target.transform.position);
				} else{
					_spriteScript.isAttacking = false;
				}
				if (_targetingScript.haveTarget && _attackingScript.isAttacking == false) {
				desiredSpot = transform.position - _targetingScript.target.transform.position;
				desiredSpot /= Vector3.Distance(transform.position,_targetingScript.target.transform.position);
				desiredSpot *= desiredRangedFromTarget;
				desiredSpot += _targetingScript.target.transform.position;

				_moverScript.SetMovement (desiredSpot);
				} else {
					_moverScript.DisableMovement ();
				}
			}
		}
	}
