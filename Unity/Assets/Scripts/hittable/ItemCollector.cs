using UnityEngine;
using System.Collections;

public class ItemCollector : MonoBehaviour
{
	public int jewelCounter;

	void Awake()
	{
		jewelCounter = 0;
	}

	public void PickJewel()
	{
		jewelCounter++;
	}
}
