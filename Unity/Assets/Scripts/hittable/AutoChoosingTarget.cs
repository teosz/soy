﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Active;

public class AutoChoosingTarget : MonoBehaviour
{
	public bool haveTarget = false; 
	public GameObject target = null;
	public Vector3 lastTargetPosition;
	public float dangerZoneRadius;
	private AutoChoosingTarget whoFoundTarget = null;
	//private float founderRank = Mathf.Infinity;
	//private AIMovement aIMovement;
	private Dictionary<GameObject,AutoChoosingTarget> listeners = new Dictionary<GameObject, AutoChoosingTarget>();

	private ObjectChecker _objectChecker;

	void Awake ()
	{
		//aIMovement = GetComponent<AIMovement> ();
		lastTargetPosition = transform.position;
		_objectChecker = GetComponent<ObjectChecker> ();
		_objectChecker.friendlyTag = this.tag;
	}
	
	void SayGoodbye (GameObject obj)
	{
		if (obj == target) {
			SendNullTarget (); //FIXME failed here
		}
		if (listeners.ContainsKey (obj)) {
			listeners[obj].SendNullTarget();
			listeners.Remove (obj);
		}
	}

	void SpeakToFriend(GameObject other)
	{
		if (target != null && other.tag == transform.tag) {
			AutoChoosingTarget otherScript = other.GetComponent<AutoChoosingTarget>();
			if (otherScript != null) {
				if(otherScript.GetTarget (target,this) == true) { //null reffrerence exception, hope it was resolved
					listeners.Add(other,otherScript);// FIXME a incercat sa adauge de 2 ori aceasi cheie, eventual pastrezi doar hash cu scripturile
				}
			}
		}
	}
	
	void OnTriggerStay(Collider other)
	{
		GetTarget (other.gameObject/*, 0*/,null);
		SpeakToFriend (other.gameObject);

	}

	public void StopListening (AutoChoosingTarget script)
	{
		listeners.Remove (script.gameObject); //initial era o lista de scripturi, nu dictionar de GO si scripturi ca acum, a ajuns asa ca sa modifc minim, cand am schimbat din liste in dictionare
	}

	public void Whisper ()
	{
		if (listeners != null) {
			foreach (AutoChoosingTarget script in listeners.Values) {
				script.GetTarget (target, this);
			}
		}
	}

	public bool GetTarget(GameObject other, /*float rank,*/ AutoChoosingTarget founder)
	{
		if (other.tag == "Player" && (target == null || Vector3.Distance (other.transform.position, transform.position) < Vector3.Distance (target.transform.position, transform.position))/* && rank < founderRank*/) {
			target = other;
			haveTarget = true;
			lastTargetPosition = other.transform.position;
			//founderRank = rank;
			if (whoFoundTarget) {
				AutoChoosingTarget oldFounderScript = whoFoundTarget.GetComponent<AutoChoosingTarget>();
				oldFounderScript.StopListening(this);
			}
			Whisper();
			whoFoundTarget = founder;
			return true;
		}
		return false;
	}

	/*
	public void GetNullTarget(float founder)
	{
		if (founder ==  whoFoundTarget) {
			target = null;
			whoFoundTarget = null;
		}

	}
	*/

	public void SendNullTarget()
	{
		/*target = null;
		haveTarget = false;
		whoFoundTarget = null;
		if (listeners != null) {
			foreach (AutoChoosingTarget script in listeners.Values) {
				script.SendNullTarget (); //caused stack overflow probably from infinit looping
			}
			listeners.Clear ();
		}*/
	}
	
	/*
	void SendTarget(Collider other)
	{
		if (other.tag == transform.tag) {
			AutoChoosingTarget otherAutoChoosingTarget = other.GetComponent<AutoChoosingTarget>();
			otherAutoChoosingTarget.GetTarget(target, whoFoundTarget+1);
		}
	}
	*/
	
	void Update ()
	{
		if (target != null && !target.gameObject.activeInHierarchy) {
			SendNullTarget();
		}



		foreach (GameObject obj in _objectChecker.leavers) {
			SayGoodbye(obj);
		}
	}
}
