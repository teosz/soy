using UnityEngine;
using System.Collections;

public class AIMovement : MonoBehaviour
{
	private NavMeshAgent nav;
	// private Rigidbody thisRigidbody;
	private SpriteManager _spriteScript;

	public bool controlRotation = false;


	//public float speed = 100;
	public Vector3 target;

	void Awake ()
	{
		// thisRigidbody = GetComponent <Rigidbody> ();
		nav = GetComponent <NavMeshAgent> ();
		nav.autoBraking = true;
		nav.updateRotation = false;

		if(GetComponentInChildren<SpriteManager> () != null)
		{
			_spriteScript = GetComponentInChildren<SpriteManager> ();
		}
	}

	void Update ()
	{ //FIXME
		if (_spriteScript!=null || _spriteScript.GetIsMoving()) { //FIXME nromal ar trebui sa fie && nu ||
			MoveToPoint();
		}
	}
	void MoveToPoint()
	{
		Vector3 difference;

		nav.SetDestination (target);

		difference = nav.steeringTarget - nav.nextPosition;
		if (controlRotation && _spriteScript.isMoving && difference != Vector3.zero) {
			transform.rotation = Quaternion.Euler(new Vector3(0, -Mathf.Atan2(difference.z,difference.x) *Mathf.Rad2Deg +90,0));
		}
	}

	public void PseudoMoveToPoint(Vector3 destination)
	{
		Vector3 difference;

		nav.SetDestination (destination);

		difference = nav.steeringTarget - nav.nextPosition;
		if (difference != Vector3.zero) {
			transform.rotation = Quaternion.Euler(new Vector3(0, -Mathf.Atan2(difference.z,difference.x) *Mathf.Rad2Deg +90,0));
		}
	}

	public void SetMovement(Vector3 newTarget)
	{
		target = newTarget;
		nav.Resume();
		controlRotation = true;
		if(_spriteScript != null)
			_spriteScript.SetIsMoving(true); //FIXME
	}

	public void DisableMovement()
	{
		nav.Stop();
		if(_spriteScript != null)
			_spriteScript.SetIsMoving(false); //FIXME
	}

	public Vector3 getDestination()
	{
		return nav.destination;
	}

	/* outdated
	public void MoveToDirection (float x, float z) // luata din tutorialul cu survival shooter, PlayerControll, am eliminat normalizarea, presupunand ca va fi facuta inainte de apelarea functiei
	{
		Vector3 movement;

		// Set the movement vector based on the axis input.
		movement = new Vector3(x, 0f, z);

		// Normalise the movement vector and make it proportional to the speed per second.
		movement = movement * speed * Time.deltaTime;

		// Move the player to it's current position plus the movement.
		thisRigidbody.MovePosition (transform.position + movement);
	}
	outdated*/
}
