﻿using UnityEngine;
using System.Collections;

public class MeleAIBrain : MonoBehaviour {

	private AIMovement _moverScript;
	private AutoChoosingTarget _targetingScript;
	private MeleAttackScript _attackingScript;
	private SpriteManager _spriteScript;

	Vector2 Vector3ToXZ(Vector3 v)
	{
		return new Vector2 (v.x, v.z);
	}

	void Awake ()
	{
		_moverScript = GetComponent<AIMovement> ();
		_targetingScript = GetComponent<AutoChoosingTarget> ();
		_attackingScript = GetComponentInChildren<MeleAttackScript> ();
		_spriteScript = GetComponentInChildren<SpriteManager> ();

	}

	void FixedUpdate()
	{
		if (_targetingScript.target) {
			if (Vector2.Distance (Vector3ToXZ (transform.position), Vector3ToXZ (_targetingScript.target.transform.position)) <= _attackingScript.attackingDistance) {
				_attackingScript.Attack (_targetingScript.target.gameObject);
			} else{
				_spriteScript.isAttacking = false;
			}
			if (_targetingScript.haveTarget) {
				_moverScript.SetMovement (_targetingScript.target.transform.position);
			} else {
				_moverScript.DisableMovement ();
			}
		}
	}
}
