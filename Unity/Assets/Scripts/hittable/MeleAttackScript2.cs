using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Active;

public class MeleAttackScript2 : MonoBehaviour
{
	private float _lastTime;
	public float deltaAtackTime = 0.25F;
	public int damage = 1;
	public bool isAttacking = false; // not sure what this is used for, anyway this shall be replaced by the ne in SpriteManager
	public float attackAnimationTime = 0.2F;
	public float attackingDistance = 0.16F;
	public float attackingObjectDistance = 0.1F;
	public float attackRange = 0.1F;
	public GameObject attackObject;
	private NavMeshAgent _nav;
	public bool _isCharging = false;
	public float chargeLevel=0;
	public float memorizedChargeLevel=0;
	private float _chargeStartingTime;
	public float chargingTime = 1.5F;
	private PlayerMovement _moverScript;
	public float slowSpeed = 0.8F;
	private float _attackStartingTime;
	private float _power;



	private ObjectChecker _objectCheckerOfAttack;
	private Vector3 _newAttackPosition;
	private SphereCollider _newAttackCollider;
	private HashSet<GameObject> _enemies = new HashSet<GameObject>();
	private HealthManager _health;
	

	private MeleAttackAnimation _anim;
	private AttackChargeSM _attackChargeSM;

	void Awake()
	{
		_lastTime = Time.time;
		_health = GameObject.Find("HealthManager").GetComponent<HealthManager> ();
		_chargeStartingTime = _attackStartingTime = Time.time - 1;
		_moverScript = GetComponent<PlayerMovement> ();
		_anim = GetComponentInChildren<MeleAttackAnimation> ();
		_objectCheckerOfAttack = GetComponentInChildren<ObjectChecker> ();
		_anim._objFinder = _objectCheckerOfAttack;
		_objectCheckerOfAttack.friendlyTag = gameObject.tag;
		_objectCheckerOfAttack.toColor = true;
	}

	void FixedUpdate()
	{
		if (_isCharging) {
			chargeLevel = Mathf.Clamp01(( Time.time - _chargeStartingTime)/ chargingTime);
			if (chargeLevel == 1){
				_moverScript.SetNewSpeed(slowSpeed);
			}
		} else {
			chargeLevel = 0;
			if(Time.time - _attackStartingTime > attackAnimationTime && isAttacking){
				EndAttack();
			}
		}

		if (isAttacking) {
			foreach (GameObject obj in _objectCheckerOfAttack.objects) { // TODO hotarastete daca pastrezi acest foreach = oricine trece prin sabie ia dmg, chiar daca la sfarsitul atacului
				_enemies.Add (obj);
			}
		} else {
			_enemies.Clear();
		}
	}

	public void ChargeAttack()
	{
		if (isAttacking == false && _isCharging == false) {
			_anim.StartCharging();
			_isCharging = true;
			_chargeStartingTime = Time.time;
		}
	}

	public void StartAttack()
	{			
		_anim.StartFastCombo ();

		_power = chargeLevel;
		memorizedChargeLevel = chargeLevel;
		_isCharging = false;
		_attackStartingTime = Time.time;
		isAttacking = true;

		_moverScript.StopMoving ();

		/*_newAttackPosition = transform.position;
		_newAttackPosition.x += attackingObjectDistance * Mathf.Sin (transform.rotation.eulerAngles.y * Mathf.Deg2Rad);
		_newAttackPosition.z += attackingObjectDistance * Mathf.Cos (transform.rotation.eulerAngles.y * Mathf.Deg2Rad);
		_newAttack = Instantiate (attackObject, _newAttackPosition, transform.rotation) as GameObject;
		_newAttackCollider = _newAttack.GetComponent<SphereCollider> ();
		_newAttackCollider.radius = 0.5F; //so it will match the sphere
		_newAttack.transform.localScale = Vector3.one * attackingDistance ;//(Vector3.Distance(transform.position, _newAttackPosition));
	*/
	}


	GameObject MyFind(GameObject toSearchIn, string name) //TODO amintestieti ce e cu functia asta
	{
		foreach (GameObject obj in toSearchIn.transform) {
			if(obj.name == name){
				return obj;
			}
		}
		return null;
	}

	public float critSizeMulti = 1.4F;

	public float fraction = 0.4f;
	public float multyplier = 2;

	public void EndAttack()
	{
		foreach (GameObject obj in _objectCheckerOfAttack.objects) {
			_enemies.Add(obj);
		}

		if (_power == 1) {
			_power = multyplier;
		} else {
			//old style _power = fraction + _power * (1F-fraction); //TODO fa asta sa aibe o singura valoare per valoare a _attackChargeSM.index
			_power = fraction;
		}

		foreach (GameObject obj in _enemies) {
			if(obj.tag != transform.tag) {
				int dmg = Mathf.RoundToInt(damage*_power);
				string sdmg = dmg.ToString();
				_health.modifyHealth( -dmg, obj); 
				GameObject _dmgTextModel = obj.GetComponentsInChildren<FlyingAndFadeingText>(true)[0].gameObject;
				//Debug.Log(obj.GetComponentInChildren<FlyingAndFadeingText>());
				GameObject _dmgText = Instantiate(_dmgTextModel, _dmgTextModel.transform.position, _dmgTextModel.transform.rotation) as GameObject;
				_dmgText.SetActive(true);
				FlyingAndFadeingText _dmgTextScript = _dmgText.GetComponent<FlyingAndFadeingText>();
				_dmgText.transform.SetParent(obj.transform, true);
				if(_power == 2){
					_dmgTextScript.NewText("Critical " +sdmg+ "!");
					_dmgText.transform.localScale *= critSizeMulti; 
				} else {
					_dmgTextScript.NewText(sdmg);
				}
				_dmgTextScript.NewText(sdmg);
				Debug.Log(obj.ToString() +"-"+ sdmg);
				//obj.transform.Find("lifebar").transform
			}
		}
	
		isAttacking = false;
		_moverScript.SetOldSpeed ();

		_anim.EndAttack ();
	}
	
}
