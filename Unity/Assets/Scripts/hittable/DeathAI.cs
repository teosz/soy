using UnityEngine;
using System.Collections;
using Active;
public class DeathAI : MonoBehaviour
{
	private Collider target;
	private bool shouldFollow;
	private AIMovement aIMovement;
	public float AttackingDistance = 0.3F;
	public int damage = 100;

	public Collider debC;
	void Awake ()
	{
		aIMovement = GetComponent<AIMovement> ();
	}

	void FixedUpdate ()
	{
		if (target!= null && Vector3.Distance (transform.position, target.transform.position) < AttackingDistance) {
			GameObject.Find("HealthManager").GetComponent<HealthManager> ().modifyHealth(-damage, target.gameObject);
		}
	}

	void OnTriggerExit (Collider other)
	{
		if(other == target) {
			shouldFollow = false;
		}
	}

	void OnTriggerStay(Collider other)
	{
		if (other.tag == "Player" && (target == null || Vector3.Distance (other.transform.position, transform.position) <= Vector3.Distance (target.transform.position, transform.position))) {
			target = other;
			shouldFollow = true;
		}
	}

	void Update ()
	{
		if (shouldFollow) {
			aIMovement.SetMovement (target.transform.position);

		} else {

			aIMovement.DisableMovement();
		}

	}
}
