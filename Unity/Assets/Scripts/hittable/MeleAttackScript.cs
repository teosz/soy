using UnityEngine;
using System.Collections;
using Active;

public class MeleAttackScript : MonoBehaviour
{
	private float _lastTime;
	public float deltaAtackTime = 0.25F;
	public int damage = 1;
	public bool isAttacking = false;
	public float attackAnimationTime = 0.2F;
	public float attackingDistance;
	public float multiply = 3; //with what it multiply the radius of _nav to get the attackingDistance
	private SpriteManager _spriteScript;
	public float distanceToMoveInAttack = 0.1F;
	public float deviationAngle = 30;
	private Vector3 _startPos;
	private Vector3 _endPos;
	private GameObject target;
	private bool _wasDamageDealt=false;
	private AutoChoosingTarget _targetScript;
	private HealthManager _health;


	private NavMeshAgent _nav;

	void Awake()
	{
		_lastTime = Time.time;
		_nav = GetComponent<NavMeshAgent> ();
		_health =  GameObject.Find("HealthManager").GetComponent<HealthManager> ();
		attackingDistance = _nav.radius * multiply;
		_spriteScript = GetComponentInChildren<SpriteManager> ();
		_targetScript = GetComponent<AutoChoosingTarget> ();
	}

	/*outdated
	void OnTriggerStay (Collider other)
	{

		if (other.tag != transform.tag && Time.time - _lastTime >= deltaAtackTime) {
			Characters.modifyHealth (-damage, other.gameObject);
			_lastTime = Time.time;
			StartCoroutine(AttackAnimatin(other.gameObject));
		}
	}
	*/

	void FixedUpdate()
	{
		if (isAttacking) {
			float timeDif = Time.time - _lastTime;
			if(!_wasDamageDealt && Vector3.Distance(_startPos,_endPos)*2/3 < Vector3.Distance(transform.position,target.transform.position)){
				_health.modifyHealth(-damage, target);
				if(target == null) {
					_targetScript.SendNullTarget();
				}
				_wasDamageDealt = true;
				_spriteScript.ChangeAttackFrame();
			}
			if(!_wasDamageDealt && timeDif >deltaAtackTime/2) {
				_spriteScript.ChangeAttackFrame();
			}

			if(timeDif > deltaAtackTime) {
				EndAttack();
			}
			else {
				//_nav.nextPosition = Vector3.Lerp(_startPos,_endPos,Mathf.Abs(Mathf.Sin ((Time.time - _lastTime)*Mathf.PI/deltaAtackTime))/2); //TODO FOLOSESTE Vector3.SmoothDamp in loc de Vectro3.Lerp (need slow fast slow) si inlocuieste magic numbers
			}
		}
	}

	public void Attack(GameObject enemy)
	{
		if (enemy.tag != transform.tag && Time.time - _lastTime >= deltaAtackTime) {
			target = enemy;
			_lastTime = Time.time;
			isAttacking = true;
			//_nav.updatePosition =false;
			_startPos = transform.position;
			_endPos = enemy.transform.position;// new Vector3(distanceToMoveInAttack*Mathf.Sin (transform.eulerAngles.y*Mathf.Deg2Rad),0,distanceToMoveInAttack*Mathf.Cos (transform.eulerAngles.y*Mathf.Deg2Rad)));
			_wasDamageDealt = false;
			_spriteScript.ChangeAttackFrame4();
		}
	}

	void EndAttack()
	{
		isAttacking = false;
		_wasDamageDealt = false;
		if (!target.gameObject.activeInHierarchy) {
			_targetScript.SendNullTarget();
		}
		//_nav.nextPosition = transform.position;
		//_nav.updatePosition = true;
		_spriteScript.EndAttack();

		transform.position = _startPos;
	}

	/* outdated
	IEnumerator AttackAction ( GameObject enemy)
	{
		isAttacking = true;
		StatusManager manager = enemy.GetComponent<StatusManager>();
		//manager.ModifyHealth(-damage);
		Characters.modifyHealth(-damage, obj);
		isAttacking = false;
	}*/
}
