using UnityEngine;
using System.Collections;

public class LookAtMainCamera : MonoBehaviour
{
	void Update()
	{
		transform.rotation = Camera.main.transform.rotation;
	}
}
