using UnityEngine;
using System.Collections;

[System.Serializable]
public class SpritesheetComponent
{
	public int staying;
	public int moving;
	public int attacking;
	public int perAttack;
}

public class SpriteManager : MonoBehaviour 
{
	private Sprite[] sprites;
	public string spriteSheetLocation;
	private SpriteRenderer spriteRenderer;
	private float rotation;
	public bool isMoving = false;
	public bool isAttacking = false;
	private int spriteIndex;
	private float specialFrameFloat = 0;
	private int specialFrameInt = 0;
	private float lastUpdateTime ;
	public float deltaMovingFrameTime = 0.18f;
	public float deltaAttackingFrameTime = 0.12f;
	private NavMeshAgent nav;
	public SpritesheetComponent numberOfFrames;
	public int attackType = 0;
	private float _attackingIndex = -1;
	//private bool _isAttackChosed = false;
	private int _chosedAttack;
	public bool specific = false;
	private int c =0;

	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer> ();
		sprites = Resources.LoadAll<Sprite>(spriteSheetLocation); //aceasta linie ia din cod poza
		lastUpdateTime = Time.time - deltaMovingFrameTime;
		nav = GetComponentInParent<NavMeshAgent> ();

	}

	void FixedUpdate()
	{
		if (!nav.pathPending)
		{
			if (nav.remainingDistance <= nav.stoppingDistance)
			{
				if (!nav.hasPath || nav.velocity.sqrMagnitude == 0f)
				{
					isMoving = false;
				}
			}
		}
	}
	
	void Update ()
	{


		if (!specific) {
			SetIndexRelativeToCameraRotation ();
			if (isAttacking && numberOfFrames.attacking > 0) {
				spriteIndex += numberOfFrames.moving;

			} else if (isMoving && numberOfFrames.moving > 0) {
				if (Time.time - lastUpdateTime > deltaMovingFrameTime) {
					specialFrameFloat++;
					lastUpdateTime = Time.time;
					specialFrameInt = Mathf.FloorToInt (Mathf.RoundToInt (Mathf.PingPong (specialFrameFloat, numberOfFrames.moving - 1))); //3 este numarul de frameuri-1
				}
			} else {
				specialFrameFloat = -1;
				specialFrameInt = -1;
				lastUpdateTime = Time.time - deltaMovingFrameTime - deltaAttackingFrameTime;
			}
			spriteRenderer.sprite = sprites[spriteIndex + specialFrameInt + 1]; //FIXME aici a depasit indexul
		}
	}
	
	public void ChangeAttackFrame()
	{
		//if (!isAttacking) {
			_attackingIndex++;
			specialFrameFloat = _attackingIndex;
			isAttacking = true;
		//}
		specialFrameFloat++;
		specialFrameInt = Mathf.FloorToInt(Mathf.RoundToInt( Mathf.PingPong(specialFrameFloat, numberOfFrames.attacking-1) ) ); //FIXME de ce am aici si round to int si floor to int

	}

	int _internalIofChangeAttackFrame4 = 0;
	public void ChangeAttackFrame4()
	{
		_internalIofChangeAttackFrame4++;
		isAttacking = true;
		specialFrameInt = _internalIofChangeAttackFrame4 % 2;
	}

	public void ChangeAttackFrame2()
	{
		//specialFrameFloat++;
		//specialFrameInt = Mathf.FloorToInt(Mathf.RoundToInt( Mathf.Repeat(specialFrameFloat, numberOfFrames.perAttack) ) );
		specialFrameInt = c%2*2;
		c++;
		isAttacking = true;
	}

	public void ChangeAttackFrame3()
	{
		//specialFrameFloat++;
		//specialFrameInt = Mathf.FloorToInt(Mathf.RoundToInt( Mathf.Repeat(specialFrameFloat, numberOfFrames.perAttack) ) );
		specialFrameInt++;
		isAttacking = true;
	}

	public void EndAttack()
	{
		isAttacking = false;
		ChangeAttackFrame2 ();

	}

	public void SetSpecific( bool newValue)
	{
		specific = newValue;
	}

	public int SetIndexRelativeToCameraRotation()
	{
		float dif = transform.parent.transform.rotation.eulerAngles.y - Camera.main.transform.rotation.eulerAngles.y;
		if(dif < 0)
			dif += 360;
		spriteIndex = Mathf.FloorToInt ((dif + 22.5f) / 45) % 8;
		spriteIndex = (spriteIndex + 4) % 8;
		spriteIndex = (8 - spriteIndex) % 8;

		spriteIndex *= numberOfFrames.moving + numberOfFrames.attacking + numberOfFrames.staying;
		return spriteIndex;
	}

	public void SetSpecificImage(int i)
	{
		specific = true;
		spriteRenderer.sprite = sprites [i];
	}

	// FIXME poate elmini astea sau folosesc set si get normale 
	public bool GetIsMoving()
	{
		return isMoving;
	}

	public void SetIsMoving(bool newValue)
	{
		isMoving = newValue;
	}
	// FIXME poate elmini astea sau folosesc set si get normale 
}
