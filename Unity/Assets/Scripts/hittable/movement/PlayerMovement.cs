using UnityEngine;
using System.Collections;

public class PlayerMovement : Photon.MonoBehaviour
{
	private NavMeshAgent _nav;
	public bool isWASD = false;
	public float defaultSpeed = 1;
	private float speed;
	private float multyplier =  1;

	private float _timeToSetSpeed;

	CameraMovement cameraMovement;
	SpriteManager spriteManager;

	Vector3 newdirection;
	Vector3 oldnewdirection = Vector3.zero;

	void Start ()
	{

		//Time.timeScale = 0.4F;


		_nav = GetComponent <NavMeshAgent> ();
		_nav.autoBraking = true;
		_nav.updateRotation = false;
		cameraMovement = Camera.main.GetComponent<CameraMovement> ();
		spriteManager = GetComponentInChildren<SpriteManager> ();
		_nav.speed = speed = defaultSpeed;
		_timeToSetSpeed = Time.time - 1;
	}

	void FixedUpdate ()
	{
		if (spriteManager.GetIsMoving() && !spriteManager.specific) {
			if(isWASD) {
				SetCharacterRotation (transform.position+newdirection);
			} else {
				SetCharacterRotation (_nav.steeringTarget);
			}
		}
		if (Time.time > _timeToSetSpeed) {
			multyplier = 1;
		}
		_nav.speed = speed * multyplier;
	}

	void LateUpdate()
	{
		StartCoroutine (cameraMovement.FollowPlayer ());
	}

	Vector2 Vector3ToXZ(Vector3 v)
	{
		return new Vector2 (v.x, v.z);
	}

	public void LookAtOnXZ(Vector3 targetPoint)
	{
		Vector3 difference = targetPoint - transform.position;
		if (difference != Vector3.zero) {
			transform.rotation = Quaternion.Euler(new Vector3(0, -Mathf.Atan2(difference.z,difference.x) *Mathf.Rad2Deg +90,0));
		}
	}

	public void MoveToPoint(Vector3 targetPoint)
	{
		_nav.SetDestination (targetPoint);
		_nav.Resume();
		isWASD = false;
		spriteManager.SetIsMoving(true);
	}

	public void StopMoving()
	{
		_nav.Stop ();
		spriteManager.SetIsMoving(false);
	}

	public void WASDMove( Vector3 relativeDirection)
	{
		if (relativeDirection != Vector3.zero) {
			_nav.Stop ();
			isWASD = true;

			//Vector3 notSoAngle = new Vector3 ( relativeDirection.x * Camera.main.transform.eulerAngles.y, 0 ,relativeDirection.z * Camera.main.transform.eulerAngles.y);

			//Vector3 newdirection = new Vector3 (Mathf.Cos (notSoAngle.x*Mathf.Deg2Rad), 0, Mathf.Sin (notSoAngle.z*Mathf.Deg2Rad));
			newdirection = Quaternion.AngleAxis (Camera.main.transform.eulerAngles.y, Vector3.up) * relativeDirection;
			_nav.Move (newdirection * _nav.speed * Time.deltaTime);
			spriteManager.SetIsMoving (true);
		} else {
			if (oldnewdirection != Vector3.zero ) {
				spriteManager.isMoving = false;
			}
			newdirection = Vector3.zero;
		}
		oldnewdirection = newdirection;
	}

	public void SetNewSpeed(float newValue)
	{
		speed = newValue;
	}

	public void SetOldSpeed()
	{
		speed = defaultSpeed;
	}

	public void ModifySpeed ( float speedMultyplier, float timeInSecounds)
	{
		multyplier = speedMultyplier;
		_timeToSetSpeed = Time.time + timeInSecounds;
	}
	void SetCharacterRotation (Vector3 target)
	{
		Vector3 difference;

		difference = target - _nav.nextPosition;
		if (difference != Vector3.zero) {
			transform.rotation = Quaternion.Euler(new Vector3(0, -Mathf.Atan2(difference.z,difference.x) *Mathf.Rad2Deg +90,0));
		}
	}

}
