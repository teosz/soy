﻿using UnityEngine;
using System.Collections;
using Active;

public class RangedAttackScript : MonoBehaviour
{
	private float _lastTime;
	public float deltaAtackTime = 0.25F;
	public int damage = 10;
	public bool isAttacking = false;
	public float attackAnimationTime = 0.2F;
	public GameObject arrow;
	public Transform arrowSpawn;
	public float Multiply = 8;
	public float attackingDistance;
	private NavMeshAgent _nav;
	private AIMovement _moverScript;
	
	void Awake()
	{
		_lastTime = Time.time;
		_nav = GetComponent<NavMeshAgent> ();
		attackingDistance = Multiply * _nav.radius;
		_moverScript = GetComponent<AIMovement> ();
	}
	
	public void Attack (Vector3 targetPosition)
	{
		if (Time.time - _lastTime >= deltaAtackTime) {
			_lastTime = Time.time;

			isAttacking = true;
			_moverScript.DisableMovement ();
			_moverScript.controlRotation = false;
			
			Vector3 difference = targetPosition - transform.position;
			if (difference != Vector3.zero) {
				transform.rotation = Quaternion.Euler(new Vector3(0, -Mathf.Atan2(difference.z,difference.x) *Mathf.Rad2Deg +90,0));
			}

			StartCoroutine (AttackAnimatin (targetPosition));
		}
	}
	
	IEnumerator AttackAnimatin (Vector3 targetPosition)
	{


		yield return new WaitForSeconds (attackAnimationTime);

		//if (difference != Vector3.zero) {
		//	transform.rotation = Quaternion.Euler(new Vector3(0, -Mathf.Atan2(difference.z,difference.x) *Mathf.Rad2Deg +90,0));
		//}

		GameObject newArrow = Instantiate (arrow, arrowSpawn.position, arrowSpawn.rotation) as GameObject;
		ShootingArrow arrowScript = newArrow.GetComponent<ShootingArrow> ();
		arrowScript.damage = damage;
		arrowScript.friendlyTag = transform.tag;
		arrowScript.lastPosition = GetComponentInChildren<SpriteRenderer> ().transform.position;
		arrowScript.SetRotation();
		isAttacking = false;
	}
}
