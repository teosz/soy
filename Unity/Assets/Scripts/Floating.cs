﻿using UnityEngine;
using System.Collections;

public class Floating : MonoBehaviour
{
	private float _startingTime;
	public float mediumY = 0.4F;
	public float minY = 0.3F;
	public float changingSpeed = 1;

	void Awake ()
	{
		_startingTime = Time.time;
	}

	void Update ()
	{
		transform.localPosition = new Vector3(0, minY + (mediumY - minY) * Mathf.Sin((Time.time - _startingTime) * changingSpeed), 0);
	}
}
