using UnityEngine;
using System.Collections;

public class JewelScript : MonoBehaviour
{
	private ItemCollector itemCollector;

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player")
		{
			itemCollector = other.GetComponent<ItemCollector>();
			itemCollector.PickJewel();
			Destroy(transform.gameObject);
		}
		if ((other.tag == "Death") && (other.GetType()==typeof(CapsuleCollider)) )
		{
			Destroy(transform.gameObject);
		}
	}
}
