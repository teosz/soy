using System;
using UnityEngine;


namespace Initialize
{
  public class Initializator:MonoBehaviour
  {

    private Int32 firstTime;
    private Int32 secondTime;
    private Action _playerDidConnect = delegate(){};
    private Action _roomCreated = delegate(){};

  	private const string roomName = "soy-dev-beta";
    public void init(Action _clientCallback, Action _onCreatedRoom)
    {
      _playerDidConnect = _clientCallback;
      _roomCreated = _onCreatedRoom;
      PhotonNetwork.ConnectUsingSettings("0.1");

    }
    void OnPhotonRandomJoinFailed(object[] codeAndMsg)
    {
      Debug.LogError(codeAndMsg);
    }
    void OnReceivedRoomListUpdate()
    {

      RoomOptions roomOptions = new RoomOptions() { isVisible = false, maxPlayers = 4 };

      PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, TypedLobby.Default);

    }
    void OnJoinedRoom()
    {
      _playerDidConnect();
    }
    void OnCreatedRoom()
    {
      _roomCreated();
    }

  }
}
