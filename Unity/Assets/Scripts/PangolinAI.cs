﻿using UnityEngine;
using System.Collections;

public class PangolinAI : MonoBehaviour
{
	
	private AIMovement _moverScript;
	private AutoChoosingTarget _targetingScript;
	private MeleAttackScript _attackingScript;
	private SpriteManager _spriteScript;
	private SpecialPangolinAnimation _animScript;
	private bool _hadTargetBefore = false;
	private NavMeshAgent _nav;

	Vector2 Vector3ToXZ(Vector3 v)
	{
		return new Vector2 (v.x, v.z);
	}

	void Awake ()
	{
		_animScript = GetComponentInChildren<SpecialPangolinAnimation> ();
		_moverScript = GetComponent<AIMovement> ();
		_targetingScript = GetComponent<AutoChoosingTarget> ();
		_attackingScript = GetComponentInChildren<MeleAttackScript> ();
		_spriteScript = GetComponentInChildren<SpriteManager> ();
		_nav = GetComponent<NavMeshAgent> ();
	}
	
	void FixedUpdate ()
	{
		if (_targetingScript.haveTarget) {
			if(!_hadTargetBefore){
				_animScript.StartAnim();
				_hadTargetBefore = true;
			} else if(!_animScript.isAnimating && Vector3.Distance(transform.position, _targetingScript.target.transform.position) > _nav.stoppingDistance ) {
				_moverScript.SetMovement (_targetingScript.target.transform.position);
			} else {
				_moverScript.DisableMovement ();

			}
		}
		//Debug.Log (_targetingScript.haveTarget);
		//Debug.Log (_targetingScript.target);
	}
}
