using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour
{
	public Transform target;
	public float notMovingRange = 0.1f;
	public float lag = 0.125f;
	public float step = 45;
	public float angularSpeed = 112.5F;
	public float loopStep;
	private Vector3 difference;
	private float radius;
	private bool isCameraRotating = false;
	private float rotateStartTime;
	private int rotateSemn;

	void Start ()
	{
		difference = transform.position - target.position;
		radius = distance (transform.position, target.position);

		//Set the camera in position
		//SetCameraRotation (0);
		//RotateCamera ();
		loopStep = 90+ Mathf.Atan2 ((target.position.z - transform.position.z) / radius, (target.position.x - transform.position.x) / radius)*Mathf.Rad2Deg;

	}

	void LateUpdate ()
	{
		if (isCameraRotating == false) {
			if (Input.GetKeyDown ("e")) {
				SetCameraRotation (1);
				RotateCamera ();
			}
			if (Input.GetKeyDown ("q")) {
				SetCameraRotation (-1);
				RotateCamera ();
			}
		}
		else {
			RotateCamera();
		}

	}

	void FixedUpdate()
	{
		//StartCoroutine(FollowPlayer());
	}

	void SetCameraRotation(int semnInput)
	{
		rotateStartTime = Time.time;
		isCameraRotating = true;
		rotateSemn = semnInput;
	}

	void RotateCamera()
	{
		if (Time.time - rotateStartTime <= lag)
			return;

		Vector3 pos;

		float currentLoopStep = loopStep + rotateSemn * angularSpeed * (Time.time - rotateStartTime);

		if (rotateSemn == 1 && currentLoopStep > loopStep + step) {
			currentLoopStep = loopStep + step;
			loopStep = currentLoopStep;
			isCameraRotating = false;
		} else if (rotateSemn == -1 && currentLoopStep < loopStep - step) {
			currentLoopStep = loopStep - step;
			loopStep = currentLoopStep;
			isCameraRotating = false;
		}


		pos.x = target.position.x + radius * Mathf.Sin(currentLoopStep * Mathf.Deg2Rad);
		pos.z = target.position.z + radius * Mathf.Cos(currentLoopStep * Mathf.Deg2Rad);
		pos.y = transform.position.y;
		transform.position = pos;
		difference = transform.position - target.position;

		transform.LookAt (target.position);



	}

	public IEnumerator FollowPlayer()
	{
		Vector3 intermediatePosition = target.position;


		yield return new WaitForSeconds(lag);

		transform.position = intermediatePosition + difference;
		yield return null;

	}

	float distance(Vector3 a, Vector3 b)
	{
			return Mathf.Sqrt( (a.x-b.x)*(a.x-b.x) + (a.z-b.z)*(a.z-b.z));
	}
}
