using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class JewelGUIScript : MonoBehaviour
{
	Text text;
	GameObject activePlayer;
	app _app;
	// Use this for initialization
	void OnEnable ()
	{
		text = GetComponent<Text> ();
		_app = GameObject.Find ("app").GetComponent<app> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		activePlayer = _app.getActivePlayer ();
		if (activePlayer != null) {
			text.text = activePlayer.GetComponent<ItemCollector>().jewelCounter.ToString ();
		}
	}
}
