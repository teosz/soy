﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AttackChargeSM : MonoBehaviour
{
	public string spritesheetPath;
	Sprite[] _spritesheet;
	GameObject activePlayer;
	Image _renderer;
	app _app;
	public int index = 0;

	// Use this for initialization
	void Awake ()
	{
		_spritesheet = Resources.LoadAll<Sprite>(spritesheetPath);
		_renderer = GetComponent<Image> ();
		_app = GameObject.Find ("app").GetComponent<app> ();
	}


	void Update ()
	{
		activePlayer = _app.getActivePlayer ();
		// Debug.Log (activePlayer);

		index = 0;
		float _chLevel = 0;
		MeleAttackScript2 _attackScript;
		if (activePlayer != null) {
			_attackScript = activePlayer.GetComponent<MeleAttackScript2> ();
			if (_attackScript != null) {
				if (_attackScript.isAttacking) {
					_chLevel = _attackScript.memorizedChargeLevel;
				} else {
					_chLevel = _attackScript.chargeLevel;
				}

				if (_chLevel != 0f) {
					index = 1 + Mathf.FloorToInt (_chLevel / 0.25F);
				}
			}
		}

		_renderer.sprite = _spritesheet [index];
	}
}
