using UnityEngine;
using System.Collections;

public class ShootingArrow : MonoBehaviour
{
	public float shoutingForce = 1;
	private Rigidbody thisRigidbody;
	public int damage;
	public string friendlyTag;
	public Vector3 lastPosition;
	public float timeUntillDisappearanceOnTerrain = 5;
	public float timeUntillDisappearanceOnPlayer = 0.2F;
	private HealthManager _health;

	void Awake ()
	{
		thisRigidbody = GetComponent<Rigidbody> ();

		_health = GameObject.Find("HealthManager").GetComponent<HealthManager> ();

		thisRigidbody.AddRelativeForce (new Vector3 (0, 0, shoutingForce));
	}

	void FixedUpdate ()
	{
		if(transform.parent = null)
			SetRotation ();
	}

	public void SetRotation ()
	{
		Vector3 dif = transform.position - lastPosition;
		if (dif != Vector3.zero) {
			transform.LookAt(transform.position - dif);
		}
		lastPosition = transform.position;
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.isTrigger == false) {
			if (((other.tag == "Player" || other.tag == "ActiveEnemy") && other.tag != friendlyTag)) {
				GameObject.Find("HealthManager").GetComponent<HealthManager> ().modifyHealth( -damage, other.gameObject);
				_health.modifyHealth (-damage, other.gameObject);
				thisRigidbody.isKinematic = true;
				transform.parent = other.transform;
				Debug.Log(other.ToString() + "  " +transform.parent.ToString());
				foreach(SpriteRenderer renderer in  GetComponentsInChildren<SpriteRenderer>() ) {
					renderer.color = Color.red; //TODO inroseste treptat
				}
				Destroy(gameObject, timeUntillDisappearanceOnTerrain);
			} else if (other.tag == "Terrain") {
				thisRigidbody.isKinematic = true;
				Destroy(gameObject, timeUntillDisappearanceOnTerrain);
			}
		}
	}
}
