﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FlyingAndFadeingText : MonoBehaviour
{
	Text text;
	public float flyingSpeed = 0.15F;
	public float fadeingSpeed = 0.75F;

	void OnEnable ()
	{
		text = GetComponent<Text> ();
	}

	public void NewText(string newText)
	{
		//text = GetComponent<Text> ();
		text.text = newText;
	}
	// Update is called once per frame
	void Update ()
	{
		transform.Translate (Vector3.up * flyingSpeed * Time.deltaTime);
		text.color = new Color( text.color.r, text.color.g, text.color.b, text.color.a - fadeingSpeed * Time.deltaTime);
		if (text.color.a <= 0) {
			Destroy(gameObject);
		}
	}
}
