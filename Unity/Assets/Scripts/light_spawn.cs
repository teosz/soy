﻿using UnityEngine;
using System.Collections;

public class light_spawn : MonoBehaviour {

	public GameObject prefab;

	IEnumerator let_there_be_light() 
	{
		yield return new WaitForSeconds(2);
		Instantiate(prefab, new Vector3(0,0,0), Quaternion.Euler(50,-30,0));
	}
	
	void Start () 
	{
		StartCoroutine(let_there_be_light());
	}

}


