﻿using UnityEngine;
using System.Collections;

public class StatusManager : MonoBehaviour
{
	public int health;
	public int maxHealth;
	public int mana;
	public int startingMana;

	void Awake ()
	{
		health = maxHealth;
		mana = startingMana;

	}

	public void ModifyHealth(int value)
	{
		health = Mathf.Clamp (health + value, 0, maxHealth);
		if (health == 0)
			this.gameObject.SetActive (false);
	}

	public void ChangeMaxHealth ( int value)
	{
		if (maxHealth + value < 1)
			maxHealth = 1;
		else
			maxHealth += value;
	}

	public bool ModifyMana(int value)
	{
		if (mana + value < 0)
			return false;
		mana += value;
		return true;
	}
}