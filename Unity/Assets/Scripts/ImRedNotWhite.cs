﻿using UnityEngine;
using System.Collections;

public class ImRedNotWhite : MonoBehaviour {

	SpriteRenderer _renderer;
	public Color red;

	void Start () {
		_renderer = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (_renderer.color == Color.white) {
			_renderer.color = red;
		}
	}
}
