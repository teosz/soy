using UnityEngine;
using System.Collections;
using Active;

public class lifebar : MonoBehaviour
{

	private GameObject player;
 	private RectTransform objectRectTransform;
	public float healthbarLenght = 0.15f;
  private int life;
  void Start ()
  {
     objectRectTransform = gameObject.GetComponent<RectTransform> ();
  }
  void Update ()
  {
    life = Characters.getHealth(transform.parent.gameObject);
    double width = life * healthbarLenght/100;
	  objectRectTransform.sizeDelta = new Vector2((float) width , (float)0.02 );
  }

}
