﻿using UnityEngine;
using System.Collections;

public class WinScript : MonoBehaviour {

	void OnTriggerEnter(Collider other)
	{
		if (!other.isTrigger && other.tag == "Friend") {
			GameObject.Find("lose_text").GetComponent<LoseConditionScript>().win();
		}
	}
}
